import React from 'react';
import ReactDOM from 'react-dom';

function SpotifyPlayer() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Spotify Player</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SpotifyPlayer;

if (document.getElementById('spotify-player')) {
    ReactDOM.render(<SpotifyPlayer />, document.getElementById('spotify-player'));
}
